$(document).ready(function () {
    let dataMenu = [{
            "nama": "Ayam Penyet"
        },
        {
            "nama": "Ayam Geprek"
        },
        {
            "nama": "Ayam Bakar"
        },
        {
            "nama": "Bebek Goreng "
        },
        {
            "nama": "Pecel Lele"
        },
        {
            "nama": "Tempe Penyet"
        }
    ];
    let dataPesanan = [];
    menu(dataMenu);
    order(dataPesanan);
    $('.btn-tambah-menu').on('click', function(){
        dataMenu.push({
            "nama" : $('#tambah_menu').val()
        });
        menu(dataMenu);
        $('#tambah_menu').val('');
        $('#tambahMenuModal').modal('hide');
    })
    $('#pesanSekarang').on('click', function(){
        dataPesanan.push({
            "menu" : $('#menu').val(),
            "jumlah" : $('#jumlah').val()
        });
        $('#menu').val('')
        $('#jumlah').val('')
        $('.pemesanan-berhasil').fadeIn();
        $('.pemesanan-berhasil').fadeOut(7000);
        order(dataPesanan);
    })
    $('#dataPesanan').on('click', '.btn-hapus' , function(){
        dataPesanan.splice($(this).data('id'), 1)
        order(dataPesanan);
    })
    $('#dataPesanan').on('click', '.btn-edit' , function(){
        $('#id_pesanan').val($(this).data('id'));
        $('#edit_menu').val(dataPesanan[$(this).data('id')].menu);
        $('#edit_jumlah').val(dataPesanan[$(this).data('id')].jumlah);
        $('#editModal').modal('show');
        order(dataPesanan);
    })
    $('.btn-update').on('click', function(){        
        dataPesanan[$('#id_pesanan').val()].menu = $('#edit_menu').val();
        dataPesanan[$('#id_pesanan').val()].jumlah = $('#edit_jumlah').val();
        $('#editModal').modal('hide');
        order(dataPesanan);
    })
});
function menu(dataMenu) {
    $('#dataMenu').empty();
    $('#menu, #edit_menu').empty();
    $.each(dataMenu, function (i, data) {
        let no = i + 1;
        $('#dataMenu').append(`
                <tr>
                    <th scope="row">${no}</th>
                    <td>${data.nama}</td>
                </tr>
            `)
        $('#menu, #edit_menu').append(`
                <option value="${data.nama}">${data.nama}</option>
            `)
    })
}
function order(dataPesanan){
    $('#dataPesanan').empty();
    $.each(dataPesanan, function (i, data) {
        $('#dataPesanan').append(`
            <tr>
                <td>${data.menu}</td>
                <td>${data.jumlah}</td>
                <td>
                    <a href="#!" class="btn btn-sm btn-dark btn-edit" data-id="${i}">Edit</a>
                    <a href="#!" class="btn btn-sm btn-danger btn-hapus" data-id="${i}">Hapus</a>
                </td>
            </tr>
            `)
    })
}